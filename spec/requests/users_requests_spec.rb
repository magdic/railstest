require 'spec_helper'

describe 'Users' do
  subject {page}
  let(:user) {FactoryGirl.create :user}

  describe 'as non-logged-in user' do

    it 'PUT user should redirect' do
      put user_path user
      response.should redirect_to signin_path
    end

    it 'GET edit user should redirect' do
      get edit_user_path user
      response.should redirect_to signin_path
    end

    it 'DELETE user should redirect' do
      delete user_path user
      response.should redirect_to root_path
    end

  end

end
