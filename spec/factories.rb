FactoryGirl.define do
  factory :user do
    sequence(:name) { |n| "Person #{n}"}
    sequence(:email) { |n| "person_#{n}@example.com" }
    password 'foobaria'
    password_confirmation 'foobaria'

    factory :admin do
      admin true
    end

    factory :email_not_verified_user do
      email_verified false
    end

    factory :email_verified_user do
      email_verified true
    end

  end

  factory :email_verification_token do
    token 'asdfasdf1234'
    user
  end

end
