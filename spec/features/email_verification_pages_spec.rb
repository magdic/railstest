require 'spec_helper'

feature 'Email verification pages' do
  subject {page}

  scenario 'new verification email page as non-signed in user' do
    visit new_email_verification_path
    current_path == root_path
  end

  scenario 'edit verification email page with invalid token' do
    visit edit_email_verification_path('i am an invalid token')
    current_path == root_path
  end

  describe 'on signup to a new account' do
    let(:user) {FactoryGirl.create :user}
    let(:new_email) {'someone@examples.com'}

    before do
      visit signup_path
      fill_in 'Name',         with: user.name
      fill_in 'Email',        with: new_email
      fill_in 'Password',     with: user.password
      fill_in 'Confirmation', with: user.password
      click_button 'Create my account'
    end

    it {should have_selector 'div.alert.alert-success'}
    it {has_css? 'title', text: full_title('Account Profile')}

    describe 'email-verification email' do
      let(:recipient_user) {User.find_by_email(new_email)}
      let(:evt) {recipient_user.email_verification_tokens.last}
      it 'should have verification token in deliveries' do
        last_email.should include recipient_user.email_verification_tokens.last.token
      end

      describe 'verification link inside email expired' do
        before do
          evt.update_column(:created_at, 1.month.ago)
          visit edit_email_verification_url(evt.token)
        end

        it {has_css? 'title', text: full_title('Email Verification Is Sent!')}
        it {should have_selector 'div.alert.alert-error'}
      end

      describe 'verification link inside email works' do
        before do
          visit edit_email_verification_url(evt.token)
        end

        it {has_css? 'title', text: full_title('Account Profile')}
        it {should have_selector 'div.alert.alert-success'}
        it {should_not have_link 'Verify Email'}

        describe 'clicking on verification link in email again' do
          before {visit edit_email_verification_url(evt.token)}

          it {has_css? 'title', text: full_title('Account Profile')}
          it {should have_selector 'div.alert.alert-success'}
          it {should_not have_link 'Verify Email'}
        end
      end

    end

  end

  describe 'while using website' do

    describe 'user has not yet verified email' do
      let(:user) {FactoryGirl.create :email_not_verified_user}
      before {sign_in user}

      it {should have_content "newPage('#{user_path user}')"}

      describe 'logs into user page' do
        let(:evt) {user.email_verification_tokens.last}
        before {visit user_path user}

        it 'should have Verify Email link in Account dropdown menu' do
          find_link('Account').has_link? 'Verify Email'
        end

        describe 'Verify Email click' do
          before {click_link 'Verify Email'}

          it 'should send email' do
            last_email.should have_content evt.token
          end

          describe 'click Verify Email again' do
            let(:old_token) {evt.token}
            before do
              visit user_path user
              user.email_verification_tokens.last.update_column(:created_at, 1.minute.ago)
              click_link 'Verify Email'
            end

            describe 'old verification link inside email still works' do
              before {visit edit_email_verification_url(old_token)}

              it {has_css? 'title', text: full_title('Account Profile')}
              it {should have_selector 'div.alert.alert-success'}
              it {should_not have_link 'Verify Email'}
            end
          end
        end
      end
    end

    describe 'user already verified email' do
      let(:user) {FactoryGirl.create :email_verified_user}
      before {sign_in user}

      describe 'verify email link not shown' do
        it {should_not have_link 'Verify Email'}
      end

    end
  end

end
