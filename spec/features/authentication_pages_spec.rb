require 'spec_helper'

feature "Authentication pages" do
  subject {page}

  describe 'signin page' do
    before {visit signin_path}

    it {has_css? 'title', text: full_title('Sign in')}
    it {should have_selector '.logo'}
    it {should_not have_link 'Sign in'}
  end

  describe 'signin' do
    before {visit signin_path}

    # TODO: Needs PhantomJS to test this.
    #describe 'with invalid information' do
    #  before {click_button 'Sign in'}
    #  it {has_css? 'title', text: full_title('Sign in')}
    #  it {should have_error_message}
    #
    #  scenario 'visiting another page after should not have any flash messages' do
    #    click_link 'logo-link'
    #    page.should not_have_error_message
    #  end
    #end

    describe 'with valid information' do
      let(:user) {FactoryGirl.create :user}

      before do
        find('input[type=email]').set     user.email
        find('input[type=password]').set  user.password
        click_button 'Sign in'
      end

      it {should have_content "newPage('#{user_path user}')"}

      describe 'logs into user account page' do
        before {visit user_path user}

        it {has_css? 'title', text: full_title('Account Profile')}
        it {should have_link 'Sign out', href: signout_path}
        it {should_not have_link 'Sign in', href: signin_path}
        it {should have_link 'Profile', href: user_path(user)}
        it {should have_link 'Settings', href: edit_user_path(user)}

        describe 'followed by signing out' do
          before {click_link 'Sign out'}
          it {has_css? 'title', text: full_title('')}
          it {should have_link 'Sign in'}
        end
      end

    end
  end

  describe 'authorization' do
    describe 'for non-signed-in users' do
      let(:user) { FactoryGirl.create :user}

      describe 'when attempting to visit edit user page' do
        before {visit edit_user_path user}

        describe 'should be redirected to sign in page' do
          it {has_css? 'title', text: full_title('Sign in')}

          describe 'after signing in' do
            before {sign_in user}

            it 'should render the desired protected page' do
              page.should have_content "newPage('#{edit_user_path user}')"
            end

            describe 'when signing in again' do
              before do
                visit edit_user_path user
                click_link 'Sign out'
                sign_in user
              end

              it 'should render the default (profile) page' do
                page.has_css? 'title', text: full_title('Account Profile')
              end
            end
          end
        end
      end

      describe 'in the Users controller' do
        describe 'visiting the edit page' do
          before {visit edit_user_path(user)}
          it {has_css? 'title', text: full_title('Sign in')}
          it {should have_selector 'div.alert.alert-notice'}
        end

        describe 'visiting the user index' do
          before {visit users_path}
          it {has_css? 'title', text: full_title('Sign in')}
        end
      end
    end

    describe 'as wrong user' do
      let(:user) {FactoryGirl.create :user}
      let(:wrong_user) {FactoryGirl.create :user, email: 'wrong@anon.net'}
      before {sign_in user}

      describe 'visiting edit page for wrong user' do
        before {visit edit_user_path wrong_user}
        it {should_not have_selector 'title', text: full_title('Account Settings')}
      end
    end

    describe 'as non-admin user' do
      let(:user) {FactoryGirl.create :user}
      let(:non_admin) {FactoryGirl.create :user}
      before {sign_in non_admin}
    end
  end

end
