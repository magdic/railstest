require 'spec_helper'

feature 'Password resets pages' do
  subject {page}
  let(:user) {FactoryGirl.create :user}

  describe 'Reset password' do
    before {visit new_password_reset_path}
    it {has_css? 'title', text: full_title('Reset password')}

    describe "sending email" do
      it 'should be able to send email' do
        user.send_password_reset
        ActionMailer::Base.deliveries.should_not be_empty
      end
    end

    describe 'submit invalid email' do
      before do
        find('input[type=email]').set 'asdf'
        click_button 'Reset my password'
      end

      it {should have_selector 'div.alert.alert-success', text: 'Email sent with password reset instructions.'}
      it {has_css? 'title', text: full_title('')}
    end

    describe 'submit wrong user\'s email' do
      let(:wrong_user) {FactoryGirl.create :user}
      before do
        find('input[type=email]').set wrong_user.email
        click_button 'Reset my password'
      end

      it {should have_selector 'div.alert.alert-success', text: 'Email sent with password reset instructions.'}
      it {has_css? 'title', text: full_title('')}
    end

    describe 'submit user\'s correct email' do
      let(:new_email) {'someone@examples.com'}

      before do
        visit signup_path
        fill_in 'Name',         with: user.name
        fill_in 'Email',        with: new_email
        fill_in 'Password',     with: user.password
        fill_in 'Confirmation', with: user.password
        click_button 'Create my account'
      end

      describe 'password reset page' do
        before do
          visit new_password_reset_path
          find('input[type=email]').set new_email
          click_button 'Reset my password'
        end

        let(:recipient_user) {User.find_by_email(new_email)}

        it 'should send an email to user' do
          page.should have_selector 'div.alert.alert-success', text: 'Email sent with password reset instructions.'
          page.has_css? 'title', text: full_title('')
          last_email.should include recipient_user.password_reset_token
        end
      end
    end
  end

  describe 'remembers last attempted login email' do
    it 'should display last attempted login email' do
      visit new_password_reset_path + "?email=" + user.email
      page.should have_selector "input#email[value='#{user.email}']"
    end
  end

end
