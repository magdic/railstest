require 'spec_helper'

feature 'Static pages' do
  subject {page}

  describe "landing page" do
    before { visit root_path }

    it {has_css? 'title', text: full_title}
    it {should have_link 'logo-link', href: root_path}
    it {should have_link 'About', href: about_path}
  end

  describe "about page" do
    before { visit about_path }

    it {has_css? 'title', text: 'About Us | Lentu'}
    it {should have_selector 'h1', text: 'About Us'}
  end

  describe "contact us page" do
    before { visit contact_path }
    it {has_css? 'title', text: 'Contact Us | Lentu'}
    it {has_css? 'h1', text: 'Contact Us'}
  end

  it 'should have the right links on the layout' do
    visit root_path

    click_link 'Sign in'
    page.has_css? 'title', text: full_title('Sign in')
    visit root_path

    click_link 'About Us'
    page.has_css? 'title', text: full_title('About Us')
    click_link 'Contact Us'
    page.has_css? 'title', text: full_title('Contact Us')
    click_link 'logo-link'

    click_link 'Sign up now!'
    page.has_css? 'title', text: full_title('Sign up')
  end

end
