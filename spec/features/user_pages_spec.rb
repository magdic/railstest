require "spec_helper"

feature "User pages" do
  subject { page }

  describe 'index' do
    let(:user) {FactoryGirl.create :user}

    before(:all) {30.times {FactoryGirl.create :user}}
    after(:all) {User.delete_all}

    before(:each) do
      sign_in user
      visit users_path
    end

    it {has_css? 'title', text: full_title('All Users')}
    it {should have_selector 'h1', text: 'All Users'}

    describe 'pagination' do
      it {has_css? 'div.pagination'}

      it 'should list each user' do
        User.paginate(page: 1).each do |user|
          page.should have_selector 'li>a', text: user.name
        end
      end
    end

    describe 'delete links' do
      it {should_not have_link 'delete'}

      describe 'as an admin user' do
        let(:admin) {FactoryGirl.create :admin}
        before do
          sign_in admin
          visit users_path
        end

        it {has_link? 'delete', href: user_path(User.first)}
        it 'should be able to delete another user' do
          expect {first(:link, 'delete').click}.to change(User, :count).by -1
        end
        it {should_not have_link 'delete', href: user_path(admin)}
      end
    end

  end

  describe 'signup page' do
    before {visit signup_path}

    it {has_css? 'title', text: full_title('Sign up')}
    it {should have_selector 'h1', text: 'Sign up'}
  end

  describe 'profile page' do
    let(:user) {FactoryGirl.create :user}
    before {visit user_path(user)}

    it {has_css? 'title', text: full_title('Account Profile')}
    it {should have_selector 'h1', text: user.name}
  end

  describe 'signing up' do
    before {visit signup_path}

    let(:submit) {'Create my account'}

    describe 'with invalid information' do
      it 'should not create a user' do
        expect {click_button submit}.not_to change(User, :count)
      end
    end

    describe 'with valid information' do
      before do
        fill_in 'Name',         with: 'Example User'
        fill_in 'Email',        with: 'anon@anon.net'
        fill_in 'Password',     with: 'foobaria'
        fill_in 'Confirmation', with: 'foobaria'
      end

      it 'should create a user' do
        expect {click_button submit}.to change(User, :count).by 1
      end

      describe 'after saving a user' do
        before {click_button submit}
        let(:user) {User.find_by_email('anon@anon.net')}

        it {has_css? 'title', text: full_title('Account Profile')}
        it {should have_selector 'div.alert.alert-success', text: 'Welcome to the Lentu Community!'}
        it {should have_link 'Sign out'}
      end
    end
  end

  describe 'edit profile' do
    let(:user) {FactoryGirl.create :user}

    before do
      sign_in user
      visit edit_user_path user
    end

    describe 'page' do
      it {has_css? 'title', text: full_title('Account Settings')}
      it {should have_selector 'h1', text: 'Account Settings'}
      it {should have_link 'Change Gravatar', href: '//gravatar.com/emails'}
    end

    describe 'with invalid information' do
      before {click_button 'Save changes'}
      it {should have_content 'error'}
    end

    describe 'with valid information' do
      let(:new_name) {'New Name'}
      let(:new_email) {'some_new@email.com'}
      before do
        fill_in 'user_name',                  with: new_name
        fill_in 'user_email',                 with: new_email
        fill_in 'user_password',              with: user.password
        fill_in 'user_password_confirmation', with: user.password
        click_button 'Save changes'
      end

      it {has_css? 'title', text: full_title('Account Profile')}
      it {should have_selector 'h1', text: new_name}
      it {should have_link 'Sign out'}
      it {should have_selector 'div.alert.alert-success'}
      specify {user.reload.name.should == new_name}
      specify {user.reload.email.should == new_email}
    end

  end

end
