include ApplicationHelper

RSpec::Matchers.define :have_error_message do |message|
  match do |page|
    page.should have_selector 'div.alert.alert-error'
  end
end

RSpec::Matchers.define :not_have_error_message do |message|
  match do |page|
    page.should_not have_selector 'div.alert.alert-error'
  end
end

def sign_in(user)
  visit signin_path
  find('input[type=email]').set     user.email
  find('input[type=password]').set  user.password
  click_button 'Sign in'
end
