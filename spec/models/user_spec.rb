require "spec_helper"

describe User do

  before {@user = User.new(
    name: 'Example User',
    email: 'anon@anon.net',
    password: 'asdfasdfasdf',
    password_confirmation: 'asdfasdfasdf'
  )}

  subject {@user}

  it {should respond_to :name}
  it {should respond_to :email}
  it {should respond_to :password_digest}
  it {should respond_to :password}              # not stored in db, created from has_secure_password
  it {should respond_to :password_confirmation} # not stored in db, created from has_secure_password
  it {should respond_to :remember_token}
  it {should respond_to :authenticate}
  it {should respond_to :admin}
  it {should respond_to :password_reset_token}
  it {should respond_to :password_reset_sent_at}
  it {should respond_to :email_verification_tokens}

  it {should be_valid}
  it {should_not be_admin}

  describe 'accessible attributes' do
    it 'should not allow access to admin' do
      expect do
        User.new admin: 1
      end.to raise_error(ActiveModel::MassAssignmentSecurity::Error)
    end
  end

  describe 'when name is not present' do
    before {@user.name = "  \t"}
    it {should_not be_valid}
  end

  describe 'when email is not present' do
    before {@user.email = "  \t"}
    it {should_not be_valid}
  end

  describe 'when name is too long' do
    before {@user.name = 'a'*101}
    it {should_not be_valid}
  end

  describe 'when name is too short' do
    before {@user.name = 'a'}
    it {should_not be_valid}
  end

  describe 'when email format is invalid' do
    it 'should be invalid' do
      addresses = %w[user@foo,com user_at_foo.org anon@foo.]
      addresses.each do |invalid_email|
        @user.email = invalid_email
        @user.should_not be_valid
      end
    end
  end

  describe 'when email format is valid' do
    it 'should be valid' do
      addresses = %w[user@foo.com A_User@anon.fb.org ace.boo@foo.jp a+b@baz.com]
      addresses.each do |valid_email|
        @user.email = valid_email
        @user.should be_valid
      end
    end
  end

  describe 'when email address is already taken' do
    before do
      user_with_same_email = @user.dup
      user_with_same_email.email = @user.email.upcase
      user_with_same_email.save
    end

    it {should_not be_valid}
  end

  describe 'when password is not present' do
    before {@user.password = @user.password_confirmation = ' '}
    it {should_not be_valid}
  end

  describe "when password doesn't match confirmation" do
    before {@user.password_confirmation = 'mismatched'}
    it {should_not be_valid}
  end

  describe 'when password confirmation is nil' do
    before {@user.password_confirmation = nil}
    it {should_not be_valid}
  end

  describe 'when password is too short' do
    before {@user.password = @user.password_confirmation = 'a' * 5}
    it {should_not be_valid}
  end

  describe 'return value of authenticate method' do
    before {@user.save}
    let(:found_user) {User.find_by_email @user.email}

    describe 'with valid password' do
      it {should == found_user.authenticate(@user.password)}
    end

    describe 'with invalid password' do
      let(:user_for_invalid_password) {found_user.authenticate('shhadflkj')}
      it {should_not == user_for_invalid_password}
      specify {user_for_invalid_password.should be_false}
    end
  end

  describe 'remember token' do
    before {@user.save}
    its(:remember_token) {should_not be_blank}
  end

  describe 'send password reset' do
    before do
      @user.save
      @user.send_password_reset
    end
    its(:password_reset_token) {should_not be_blank}
    its(:password_reset_sent_at) {should_not be_blank}
  end

  describe 'email_verification_tokens associations' do

    before {@user.save}
    let(:evt) { FactoryGirl.create(:email_verification_token, user: @user) }

    it 'should have the right email verification token' do
      @user.email_verification_tokens.should == [evt]
    end

    it 'should destroy associations on user destruction' do
      evts = @user.email_verification_tokens
      @user.destroy
      evts.each do |evt|
        EmailVerificationToken.find_by_id(evt.id).should be_nil
      end
    end
  end

end
