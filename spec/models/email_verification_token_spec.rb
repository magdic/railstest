require 'spec_helper'

describe EmailVerificationToken do

  let(:user) {FactoryGirl.create :user}

  before do
    @evt = user.email_verification_tokens.build token: 'asdfasdf1234'
    user.save
  end

  subject {@evt}

  it {should respond_to :user_id}
  it {should respond_to :user}
  its(:user) {should == user}
  it {should respond_to :token}

  it {should be_valid}

  describe 'mass assignment protected attributes' do
    it 'should not allow access to user' do
      expect do
        EmailVerificationToken.new(user_id: 1)
      end.to raise_error(ActiveModel::MassAssignmentSecurity::Error)
    end
  end

  describe 'when user is not present' do
    before {@evt.user_id = nil}
    it {should_not be_valid}
  end

  describe 'with blank token' do
    before { @evt.token = '  ' }
    it { should_not be_valid }
  end

  describe 'with nil token' do
    before { @evt.token = nil }
    it { should_not be_valid }
  end

end
