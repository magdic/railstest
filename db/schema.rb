# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121205042153) do

  create_table "email_verification_tokens", :force => true do |t|
    t.integer  "user_id"
    t.string   "token"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "email_verification_tokens", ["token"], :name => "index_email_verification_tokens_on_token", :unique => true
  add_index "email_verification_tokens", ["user_id", "token"], :name => "index_email_verification_tokens_on_user_id_and_token", :unique => true
  add_index "email_verification_tokens", ["user_id"], :name => "index_email_verification_tokens_on_user_id"

  create_table "omni_auth_provider_credentials", :force => true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.integer  "latitude"
    t.integer  "longitude"
    t.string   "city"
    t.string   "state"
    t.string   "type"
    t.string   "token"
    t.datetime "token_expires_at"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "omni_auth_provider_credentials", ["provider", "uid"], :name => "index_omni_auth_provider_credentials_on_provider_and_uid", :unique => true

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "remember_token"
    t.boolean  "admin",                  :default => false
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.boolean  "email_verified",         :default => false
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["email_verified"], :name => "index_users_on_email_verified"
  add_index "users", ["password_reset_sent_at"], :name => "index_users_on_password_reset_sent_at"
  add_index "users", ["password_reset_token"], :name => "index_users_on_password_reset_token"
  add_index "users", ["remember_token"], :name => "index_users_on_remember_token"

end
