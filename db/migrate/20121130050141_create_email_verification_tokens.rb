class CreateEmailVerificationTokens < ActiveRecord::Migration
  def change
    create_table :email_verification_tokens do |t|
      t.integer :user_id
      t.string  :token, unique: true
      t.timestamps
    end

    add_index :email_verification_tokens, :user_id
    add_index :email_verification_tokens, :token, unique: true
    add_index :email_verification_tokens, [:user_id, :token], unique: true
  end
end
