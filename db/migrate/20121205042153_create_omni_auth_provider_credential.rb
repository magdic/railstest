class CreateOmniAuthProviderCredential < ActiveRecord::Migration
  def change
    create_table :omni_auth_provider_credentials do |t|
      t.integer   :user_id

      t.string    :provider
      t.string    :uid
      t.string    :name
      t.string    :first_name
      t.string    :middle_name
      t.string    :last_name
      t.integer   :latitude
      t.integer   :longitude
      t.string    :city
      t.string    :state
      t.string    :type
      t.string    :token
      t.datetime  :token_expires_at
      t.timestamps
    end

    add_index :omni_auth_provider_credentials, [:provider, :uid], unique: true
  end
end
