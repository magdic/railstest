class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string    :name
      t.string    :email
      t.string    :password_digest
      t.string    :remember_token
      t.boolean   :admin, default: false
      t.string    :password_reset_token
      t.datetime  :password_reset_sent_at
      t.boolean   :email_verified, default: false

      t.timestamps
    end

    add_index :users, :email, unique: true
    add_index :users, :remember_token
    add_index :users, :password_reset_token
    add_index :users, :password_reset_sent_at
    add_index :users, :email_verified
  end
end
