Lentu
-----

Project *Lentu* uses a number of open source projects to make it work properly:
- Ruby 1.9.3
- Rails 3.2.13
- HTML5
- CSS3 w/ SASS & Compass
- Javascript / jQuery

Hosting
-------
- Heroku

This application demonstrates a beautiful login page with animated
context boxes and uses unobstrusive javascript in an AJAX-based
request-response model.

Visit [*Lentu*](http://lentu.herokuapp.com) (http://lentu.herokuapp.com) to see this application in action.

License
-------
MIT
