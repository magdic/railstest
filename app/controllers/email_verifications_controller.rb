class EmailVerificationsController < ApplicationController
  include EmailVerificationsHelper

  def new
    @send_status = false

    if current_user
      @user = current_user
      @send_status = @user.send_email_verification
    end

    respond_to do |format|
      format.js {render :new}
    end
  end

  def edit
    return already_verified current_user if current_user && current_user.email_verified

    @evt = EmailVerificationToken.find_by_token(params[:id])
    if @evt
      @user = @evt.user
      return already_verified @user if @user.email_verified

      if @evt.created_at < 50.hours.ago
        flash[:error] = "Email verification link has expired, we are now sending you new verification email."
      else @evt.created_at >= 50.hours.ago
        @user.update_column(:email_verified, true)
        flash[:success] = "Congratulations, your email account is now verified!"
      end
      redirect_to @user
    else
      flash[:error] = "We were unable to verify your email with the given link.  Please request another verification email from the Account dropdown menu."
      redirect_to root_path
    end
  end

end
