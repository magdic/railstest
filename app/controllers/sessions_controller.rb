class SessionsController < ApplicationController

  def new
    @email_default_value = params[:email]
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    # If served via JS, then regular login.  Else then logging in via OmniAuth.
    # Could just check for omniauth.auth in env also.
    if env["omniauth.auth"]
      if current_user
        render text: "Found existing user #{current_user.name}, linking now"
      else
        render text: 'Failed to find logged in user'
      end
      #user = User.from_omniauth env["omniauth.auth"]
      #raise user.to_yaml
    else
      user = User.find_by_email(params[:session][:email])
      if user && user.authenticate(params[:session][:password])
        sign_in user
        #redirect_back_or user
        js_redirect_back_or( user_path user )
      else
        #flash.now[:error] = "Sign in attempt failed.  Please check your email and password."
        #render 'new'
      end
    end
  end

  def destroy
    sign_out
    redirect_to root_path
  end

end
