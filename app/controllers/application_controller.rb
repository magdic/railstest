class ApplicationController < ActionController::Base
  protect_from_forgery

  include SessionsHelper

  def js_redirect_to(path)
    render js: %(window.location.pathname='#{path}') and return
  end

end
