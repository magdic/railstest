class StaticPagesController < ApplicationController
  def home
    respond_to do |format|
      format.html
      format.js   { render js: %(newPage('#{root_path}')) }
    end
  end

  def about
  end

  def contact
  end

end
