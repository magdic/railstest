class PasswordResetsController < ApplicationController
  def new
    @email_default_value = params[:email]
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    user = User.find_by_email(params[:email])
    user.send_password_reset if user
    flash[:success] = "Email sent with password reset instructions."
    redirect_to signin_path
  end

  def edit
    @user = User.find_by_password_reset_token!(params[:id])
  end

  def update
    @user = User.find_by_password_reset_token!(params[:id])
    if @user.password_reset_sent_at < 10.hours.ago
      redirect_to new_password_reset_path, alert: "Password reset has expired."
    elsif @user.update_attributes(params[:user])

      flash[:success] = "Password has been reset!"
      redirect_to root_url
    else
      render :edit
    end
  end

end
