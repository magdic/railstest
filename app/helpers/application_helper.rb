module ApplicationHelper

  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    if page_title.empty?
      "#{ENV['APPLICATION_SLOGAN']} | #{ENV['APPLICATION_NAME']}"
    else
      "#{page_title} | #{ENV['APPLICATION_NAME']}"
    end
  end

  def defer_load_javascript_file(filename = '')
    return '' if filename.blank?

    "getScript('#{asset_path(filename)}', function(){});"
  end

end
