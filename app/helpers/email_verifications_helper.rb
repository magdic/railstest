module EmailVerificationsHelper

  def already_verified(user)
    flash[:success] = "Your email is already verified, you do not need to verify again."
    redirect_to user
  end

end
