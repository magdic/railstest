class UserMailer < ActionMailer::Base
  default from: "lentu-mailman@lentu.us"

  def password_reset(user)
    @user = user

    if Rails.env.test?
      ActionMailer::Base.deliveries = [user.password_reset_token]
      return true
    end

    begin
      RestClient.post "https://api:#{ENV['MAILGUN_API_KEY']}"\
        "@api.mailgun.net/v2/#{ENV['MAILGUN_LENTU_DOMAIN']}/messages",
        from:     "#{ENV['LENTU_EMAIL_SENDER_NAME']} #{ENV['LENTU_EMAIL_SENDER_EMAIL']}",
        to:       user.email,
        subject:  "Password Reset",
        text:     "#{render_to_string(template: 'user_mailer/password_reset', formats: [:text])}"
    rescue => e
      puts "#{e.inspect}" if Rails.env.development?
      return false
    end

    true
  end

  def email_verification(user)
    @user   = user
    @token  = @user.email_verification_tokens.last.token

    if Rails.env.test?
      ActionMailer::Base.deliveries = [@token]
      return true
    end

    begin
      RestClient.post "https://api:#{ENV['MAILGUN_API_KEY']}"\
        "@api.mailgun.net/v2/#{ENV['MAILGUN_LENTU_DOMAIN']}/messages",
        from:     "#{ENV['LENTU_EMAIL_SENDER_NAME']} #{ENV['LENTU_EMAIL_SENDER_EMAIL']}",
        to:       user.email,
        subject:  "Email Verification",
        text:     "#{render_to_string(template: 'user_mailer/email_verification', formats: [:text])}"
    rescue => e
      puts "#{e.inspect}" if Rails.env.development?
      return false
    end

    true
  end

end
