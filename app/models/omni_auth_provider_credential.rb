class OmniAuthProviderCredential < ActiveRecord::Base
  attr_accessible :provider, :uid, :name

  belongs_to :user

  validates :user_id,   presence: true
  validates :provider,  presence: true
  validates :uid,       presence: true
  validates :name,      presence: true

end
