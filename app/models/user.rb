class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
      record.errors[attribute] << (options[:message] || "is not an email")
    end
  end
end

class User < ActiveRecord::Base
  has_secure_password # creates :password & :password_confirmation
  attr_accessible :name, :email, :password, :password_confirmation

  has_many :email_verification_tokens,      dependent: :destroy
  has_many :omni_auth_provider_credentials, dependent: :destroy

  before_save {|user| user.email = user.email.downcase}
  before_save {create_authentication_token(:remember_token)}

  validates :name,                  presence: true,
                                    length: {maximum: 100, minimum: 2}

  validates :email,                 presence: true,
                                    email: true,
                                    uniqueness: {case_sensitive: false}

  validates :password,              presence: true,
                                    length: {minimum: 8}

  validates :password_confirmation, presence: true,
                                    length: {minimum: 8}

  def send_email_verification
    return 'already verified' if self.email_verified

    last_evt = self.email_verification_tokens.last
    return 'too soon' if last_evt && last_evt.created_at > 2.seconds.ago

    self.email_verification_tokens.create!(token: EmailVerificationToken.create_token)
    UserMailer.email_verification(self)
  end

  def send_password_reset
    # TODO: come Rails 4.0, use update_columns
    update_column :password_reset_token, create_authentication_token(:password_reset_token)
    update_column :password_reset_sent_at, Time.zone.now

    UserMailer.password_reset(self)
  end

  def self.from_omniauth(auth)
    cred = OmniAuthProviderCredential.where(auth.slice("provider", "uid")).first
    cred ? cred.user : create_from_omniauth(auth)
  end

  def self.create_from_omniauth(auth)
    user = self.create! do |user|
      user.name   = auth["info"]["name"]
    end

    cred = self.omni_auth_provider_credentials.new do |cred|
      cred.provider = auth["provider"]
      cred.uid      = auth["uid"]
      cred.name     = auth["info"]["name"]

      #t.string    :provider
      #t.string    :uid
      #t.string    :name
      #t.string    :first_name
      #t.string    :middle_name
      #t.string    :last_name
      #t.integer   :latitude
      #t.integer   :longitude
      #t.string    :city
      #t.string    :state
      #t.string    :type
      #t.string    :token
      #t.datetime  :token_expires_at
    end
    cred.save(validate: false)
    cred.user
  end

  private

    def create_authentication_token(column)
      self[column] = SecureRandom.urlsafe_base64
    end
end
