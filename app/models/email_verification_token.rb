class EmailVerificationToken < ActiveRecord::Base
  attr_accessible :token

  belongs_to :user

  validates :user_id, presence: true
  validates :token,   presence: true

  def self.create_token
    SecureRandom.urlsafe_base64
  end
end
