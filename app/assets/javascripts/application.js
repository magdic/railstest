// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery-ujs-2012-10-27
//= require jquery.jrumble.min
//= require jquery.mousewheel-3.0.6
//= require jquery.fancybox
//= require uservoice
//= require google_analytics
//= require_self
//= require_directory .

var destroyElement = function (e) {
  $(e).remove();
};

var stopShake = function (target) {
  target.trigger('stopRumble');
  target.css({
    '-webkit-transform':'none',
    '-moz-transform':'none',
    '-ms-transform':'none',
    '-o-transform':'none',
    'transform':'none',
    'filter': "progid:DXImageTransform.Microsoft.gradient(gradientType=0, startColorstr='#B3FFFFFF', endColorstr='#73FFFFFF')"
  });
};

var shake = function (target) {
  target.trigger('startRumble');
  setTimeout(function(){stopShake(target)}, 300);
};

$('.fades-on-click').click(function () {
  $(this).fadeTo(200, 0, "linear").slideUp(200, "linear", function () {destroyElement(this);});
});
