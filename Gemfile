source 'http://rubygems.org'
ruby "1.9.3"

gem 'rails'

gem "jquery-rails-cdn" # jQuery & jQuery UJS w/ CDN support: https://github.com/kenn/jquery-rails-cdn

gem 'will_paginate'
gem 'bootstrap-will_paginate' # We've manually included bootstrap instead of using gem.


# Use sqlite3 as the database for Active Record
gem 'sqlite3'
#gem 'pg', :platforms => [:ruby] # Used by Heroku as default db
gem 'bcrypt-ruby'

gem "unicorn", :platforms => :ruby # Install on *nix systems only
gem 'puma' # Fast, no bs webserver good for dev

# gem 'newrelic_rpm' # Application performance measurement stats.

gem 'memcachier'
gem 'dalli'
gem 'cache_digests'

# For configuring Rails apps using a private unversioned file (like application.yml). https://github.com/laserlemon/figaro
gem 'figaro' # To use: rails g figaro:install

# Needed to make RESTful API calls
gem 'rest-client'
# gem 'multimap'

gem 'omniauth'
gem 'omniauth-dwolla'

gem 'capistrano'

group :development, :test do
  gem 'rspec-rails'
  gem 'capybara'
  gem 'guard-rspec'
  gem 'guard-spork'
  gem 'spork'
  gem 'faker'
  gem 'better_errors'
end

group :test do
  gem 'factory_girl_rails'
  gem 'database_cleaner'
  gem 'launchy' # allows save_and_open_page method in rspec
  # gem 'rb-fsevent', :require => false
  gem 'rb-inotify', :platforms => [:ruby]
  gem 'wdm', platforms: [:mingw, :mswin]
end

# Gems used only for assets and not required
# in production environments by default.
gem 'sass-rails'
group :assets do
  gem 'compass-rails'
  gem 'closure-compiler'
  gem 'oily_png'
end
