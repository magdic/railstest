# Load the rails application
require File.expand_path('../application', __FILE__)

# Lentu::Application.configure do
#   config.action_mailer.delivery_method = :smtp
#   config.action_mailer.smtp_settings = {
#     address:              ENV['MAILER_ADDRESS'],
#     port:                 587,
#     domain:               'lentu.us',
#     user_name:            ENV['MAILER_USERNAME'],
#     password:             ENV['MAILER_PASSWORD'],
#     authentication:       'plain',
#     enable_starttls_auto: true
#   }
# end

# Initialize the rails application
Lentu::Application.initialize!
