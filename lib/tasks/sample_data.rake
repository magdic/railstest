namespace :db do
  desc 'Fill database with sample data'
  task populate: :environment do
    first_user = User.create!(name: "First User",
                 email: 'anon@anon.net',
                 password: 'asdfasdf',
                 password_confirmation: 'asdfasdf')
    first_user.toggle!(:admin)

    99.times do |n|
      name = Faker::Name.name
      email = "example-#{n+1}@railstutorial.org"
      password = 'password'
      User.create!(name: name,
                   email: email,
                   password: password,
                   password_confirmation: password)
    end
  end
end
